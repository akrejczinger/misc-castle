#!/bin/zsh

# Start mpd if not running already
if [ ! -s ~/.config/mpd/pid ]; then
    mpd ~/.config/mpd/mpd.conf
    mpc stop
fi
