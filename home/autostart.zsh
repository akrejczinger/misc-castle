#!/bin/zsh

# if dropbox is running, assume setup already ran once, exit.
if pgrep "dropbox" > /dev/null
then
    echo "Already ran once, exiting."
    exit
fi

# set up modified colemak layout
if [ -s ~/.xmodmap ]; then
    setxkbmap us -variant colemak
    xmodmap ~/.xmodmap
fi

# Prevent screen turning off when idle
sleep 1; xset s off -dpms

# configuration
xrdb -merge ~/.Xresources

# Start screensaver for lock screen
xscreensaver -no-splash &

# daemons
numlockx &
compton -b &
thunar --daemon &
nm-applet &
volnoti &
redshift &
dropbox start &
conky &
keepass2 -minimize &
# mpd &
# mpc stop &

~/.config/awesome/scripts/touchpad.sh Off
